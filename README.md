To run the project:

1. Clone the project

    git clone https://Hysterus@bitbucket.org/Hysterus/dev-hunter.git

2. Go to the dist directory

    cd dev-hunter/dist

3. Run a simple web server. For example:

    python -m SimpleHTTPServer 8000


You could also start a webpack-dev-server, however it requires installing all dependencies.

1. Install dependencies

    cd dev-hunter && npm install

2. Run dev server

    npm run start-dev


At the end open in the browser: http://localhost:8000