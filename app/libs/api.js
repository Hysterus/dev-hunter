import axios from "axios"

const GithubApiUrl = 'https://api.github.com'
const organization = "angular"
const perPage = 100

export const GithubApi = axios.create({
    baseURL: 'https://api.github.com'
})

export const GithubApiUrls = {
    repos: `${GithubApiUrl}/orgs/${organization}/repos?per_page=${perPage}`,
    userDetails: login => `${GithubApiUrl}/users/${login}`,
    repo: (owner, name) => `${GithubApiUrl}/repos/${owner}/${name}`
}

