import R from "ramda"
import {logger} from "./debug"

export const parseLinkHeader = R.compose(
    R.reduce( (acc, pair) => ({...acc, [pair[0]]: pair[1]}) , {} ),
    R.map(([linkWrapped, rel]) => [ R.match(/rel="(.*)"/, rel)[1], R.match(/<(.*)>/, linkWrapped)[1] ]),
    R.map(R.split(";")),
    R.split(',')
)
