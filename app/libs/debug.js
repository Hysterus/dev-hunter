
export const logger = (func) => (...args) => {
    console.group(`${func.name} invoked`)
    console.log("inputs:", args)

    const returnVal = func(...args)

    console.log("output:", returnVal)
    console.groupEnd()
    return returnVal
}