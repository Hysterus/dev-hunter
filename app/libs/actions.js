import R from "ramda"

import {parseLinkHeader} from "./http";
import {GithubApi} from "../libs/api"

export const simpleCatch = (dispatch, rejectAction, ...args) => err => {
    dispatch(rejectAction(err, ...args))
    throw err
}

export const fetchActions = name => ({
    init: (args = {}) => ({type: `FETCH_${name}_STARTED`, ...args}),
    reject: (err, args = {}) => ({type: `FETCH_${name}_REJECTED`, payload: err, ...args}),
    finish: (data, args = {}) => ({type: `FETCH_${name}_FINISHED`, payload: data, ...args}),
})

export const fetchWithPaginationAction = (standardActions, defaultURL) => function fetch(URL = defaultURL, maxPages = Infinity, acc = [], page = 1) {
    return dispatch => {
        page === 1 && dispatch(standardActions.init())

        return GithubApi.get(URL, {
            params: {per_page: 100}
        })
            .then(response => ({
                data: R.concat(acc, response.data),
                nextPage: response.headers.link && parseLinkHeader(response.headers.link).next
            }))
            .then(response => {
                if (response.nextPage && page < maxPages) {
                    return dispatch(fetch(response.nextPage, maxPages, response.data, page + 1))
                }

                dispatch(standardActions.finish(response.data))
                return response.data
            })
            .catch(simpleCatch(dispatch, standardActions.reject))
    }
}

export const simpleFetchAction = standardActions => URL => dispatch => {
    dispatch(standardActions.init())
    return GithubApi.get(URL)
        .then(R.prop('data'))
        .then(R.tap(data => dispatch(standardActions.finish(data))))
        .catch(simpleCatch(dispatch, standardActions.reject))
}

export const getFromStore = (pick, ...args) => new Promise((resolve, reject) => {
    try {
        const value = pick(...args)
        return value !== undefined ? resolve(value) : reject(new Error("Could not load value from cache"))
    }
    catch(err){
            return reject(err)
    }
})

