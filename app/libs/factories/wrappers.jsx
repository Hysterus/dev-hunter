import React from "react"
import {Guard} from "../../components/utils/Guard"

export const withSpinnerAndError = options => Component => props =>
    <Guard {...props} options={options} render={props => <Component {...props}/>}/>