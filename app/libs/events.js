export const noop = () => {}

export const eventPreventing = func => e => {
    func()

    e.preventDefault();
    e.stopPropagation();
}