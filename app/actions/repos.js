import R from "ramda"

import {GithubApi, GithubApiUrls} from "../libs/api"
import {fetchWithPaginationAction, fetchActions, getFromStore, simpleCatch} from "../libs/actions"


export const fetchRepos = fetchWithPaginationAction(fetchActions("REPOS"), GithubApiUrls.repos)


const reposForContributorActions = fetchActions("REPOS_FOR_CONTRIBUTOR")

export const fetchReposForContributor = ({login, repos_url}) => (dispatch, getState) => {
    dispatch(reposForContributorActions.init({login}))

    return getFromStore(
        R.path(['contributorsRepos', login]),
        getState().repositories
    ).then(repos => dispatch(reposForContributorActions.finish(repos, {source: "CACHE", login})))
        .catch(() => dispatch(fetchRepos(repos_url, 1))
            .then(repos => dispatch(reposForContributorActions.finish(repos, {login})))
            .catch(simpleCatch(dispatch, reposForContributorActions.reject, {login}))
        )

}

const actionsForDetails = fetchActions("REPO_DETAILS")

export const fetchRepoDetails = (owner, name) => (dispatch, getState) => {
    dispatch(actionsForDetails.init({owner, name}))

    return getFromStore(
        R.path(['reposDetails', owner, name]),
        getState().repositories
    ).then(R.tap(repo => dispatch(actionsForDetails.finish(repo, {source: "CACHE", owner, name}))))
        .catch(() => GithubApi.get(GithubApiUrls.repo(owner, name))
            .then(R.prop("data"))
            .then(R.tap(repo => dispatch(actionsForDetails.finish(repo, {owner, name}))))
            .catch(simpleCatch(dispatch, actionsForDetails.reject))
        )
}