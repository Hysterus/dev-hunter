import {GithubApiUrls} from "../libs/api"
import * as R from "ramda";

import {fetchWithPaginationAction, fetchActions, simpleFetchAction, simpleCatch, getFromStore} from "../libs/actions"
import {fetchRepos} from "./repos";

const limitReposToAvoidRateLimiting = R.take(5)
const limitContributorsToAvoidRateLimiting = R.take(15)

const mergeSameContributor = R.mergeWithKey((key, left, right) => key === "contributions" ? left + right : right)

const mergeContributors = R.compose(
    R.map(R.reduce(mergeSameContributor, {})),
    R.groupWith((a, b) => a.id === b.id),
    R.sortBy(R.prop('id'))
)


export const fetchContributorsForRepo = fetchWithPaginationAction(fetchActions("CONTRIBUTORS"))
export const fetchContributor = simpleFetchAction(fetchActions("CONTRIBUTOR_DETAILS"))


const fetchActionsForOrg = fetchActions("CONTRIBUTORS_FOR_ORG")

export const fetchContributorsForOrg = () => (dispatch, getState) => {
    dispatch(fetchActionsForOrg.init())

    return getFromStore(
        R.prop("all"),
        getState().contributors
    ).then(R.tap(data => dispatch(fetchActionsForOrg.finish(data, {source: "CACHE"}))))
        .catch(() => dispatch(fetchRepos())
            .then(limitReposToAvoidRateLimiting)
            .then(R.map(R.prop("contributors_url")))
            .then(R.map(R.compose(dispatch, fetchContributorsForRepo)))
            .then(promises => Promise.all(promises))
            .then(R.flatten)
            .then(mergeContributors)
            .then(limitContributorsToAvoidRateLimiting)
            .then(R.map(contributor => dispatch(fetchContributor(contributor.url)).then(details => [contributor, details])))
            .then(promises => Promise.all(promises))
            .then(R.map(([a, b]) => R.merge(a, b)))
            .then(R.tap(data => dispatch(fetchActionsForOrg.finish(data))))
            .catch(simpleCatch(dispatch, fetchActionsForOrg.reject))
        )
}


export const changeContributorsSorting = (prop, type) => ({type: 'CHANGE_CONTRIBUTORS_ORDERING', payload: {prop, type}})


const fetchActionsForDetails = fetchActions("CONTRIBUTOR_DETAILS_PAGE")

export const fetchContributorDetails = login => (dispatch, getState) => {
    dispatch(fetchActionsForDetails.init({login}))

    return getFromStore(
        state => state.all.filter(elem => elem.login === login)[0],
        getState().contributors
    ).then(R.tap(contributor => dispatch(fetchActionsForDetails.finish(contributor, {source: "CACHE"}))))
        .catch(() => dispatch(fetchContributor(GithubApiUrls.userDetails(login)))
            .then(R.tap(R.compose(dispatch, fetchActionsForDetails.finish)))
            .catch(simpleCatch(dispatch, fetchActionsForDetails.reject))
        )
}

const contributorsForRepoActions = fetchActions("CONTRIBUTORS_FOR_REPO")
export const fetchContributorsForRepoWithStore = URL => (dispatch, getState) => {
    dispatch(contributorsForRepoActions.init({URL}))

    return getFromStore(
        R.path(["reposContributors", URL]),
        getState().contributors
    ).then(R.tap(repo => dispatch(contributorsForRepoActions.finish(repo, {source: "CACHE", URL}))))
        .catch(() => dispatch(fetchContributorsForRepo(URL, 1))
            .then(R.tap(repo => dispatch(contributorsForRepoActions.finish(repo, {source: "CACHE", URL}))))
            .catch(simpleCatch(dispatch, contributorsForRepoActions.reject))
        )

}