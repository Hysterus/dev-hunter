import React from "react"

import "./spinner.scss"

const Spinner = ({className = ""}) => (
    <div className={"spinner " + className}>
        <i className={"fa fa-spinner spinner__image"} aria-hidden="true"/>
    </div>
)

export default Spinner;