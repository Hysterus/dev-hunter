import React from "react"
import R from "ramda"
import {eventPreventing, noop} from "../../libs/events"

import "./cards.scss"

const invertOrderingType = type => type === "asc" ? "desc" : "asc"
const defaultOrdering = "desc"

const OrderIndicator = ({type = "asc"}) => (
    type === 'asc' ?
        <span className="fa fa-sort-asc cards-group__legend__label__asc" aria-hidden="true"/> :
        <span className="fa fa-sort-desc cards-group__legend__label__desc" aria-hidden="true"/>
)

const Label = ({onClick, children}) => (
    <span className={`cards-group__legend__label table__cell ` + (onClick ? "clickable" : "")}
          onClick={eventPreventing(onClick)}>
        {children}
    </span>
)

const getNewOrdering = (chosenProp, oldOrdering) =>
    chosenProp === oldOrdering.prop ?
        {prop: chosenProp, type: invertOrderingType(oldOrdering.type)} :
        {prop: chosenProp, type: defaultOrdering}

const onLabelClick = (handler, prop, currentOrdering) => () => {
    const newOrdering = getNewOrdering(prop, currentOrdering)
    handler(newOrdering.prop, newOrdering.type)
}

export const CardContent = ({children, className = ""}) => (
    <div className={`card__content ${className}`}>{children}</div>
)

export const Card = ({children, onClick = noop, className=""}) => (
    <div className={`card ${className} ` + (onClick === noop ? "" : "clickable")} onClick={eventPreventing(onClick)}>
        {children}
    </div>
)

export const CardsList = ({children}) => <div className="cards-group__list">{children}</div>
export const CardsListLegend = ({labelsList, onChangeOrder, ordering={}, className = ""}) => (
    <div className={"cards-group__legend table " + className}>
        {labelsList
            .map(field => ({...field, name: R.propOr(R.prop("prop", field), "visible", field)}))
            .map(field => (
                <Label key={field.prop} onClick={onChangeOrder && onLabelClick(onChangeOrder, field.prop, ordering)}>
                    {field.name} {ordering.prop === field.prop && <OrderIndicator type={ordering.type}/>}
                </Label>
            ))}
    </div>
)

export const CardsGroup = ({children, className = ""}) => (
    <div className={"cards-group " + className}>
        {children}
    </div>
)