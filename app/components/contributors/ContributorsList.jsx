import React from "react"
import "./contributorsList.scss"
import R from "ramda";

import {CardContent, Card, CardsGroup, CardsList, CardsListLegend} from "../cards/Cards"
import Avatar from "../avatar/Avatar"

const defaultFields = [
    {prop: "contributions"},
    {prop: "followers"},
    {prop: "public_repos", visible: "Repositories"},
    {prop: "public_gists", visible: "Gists"},
]

export const ContributorsList = ({children, ordering = {}, onChangeOrder, fields=defaultFields, legendClassName=""}) => (
    <CardsGroup>
        <CardsListLegend className={`contributors-legend contributors-legend--${fields.length}-fields noselect ${legendClassName} `} ordering={ordering} labelsList={fields}
                         onChangeOrder={onChangeOrder}/>
        <CardsList>
            {children}
        </CardsList>
    </CardsGroup>
)

export const Contributor = ({data: contributor, onClick, fields=defaultFields, detailsClassName=""}) => (
    <Card onClick={onClick}>
        <CardContent className="card__content--left">
            <Avatar src={contributor.avatar_url} className="avatar--small float-left contributor-avatar"/>
            <span className="card__username truncated">{contributor.login}</span>
        </CardContent>
        <CardContent className={`card__content--right table contributors-stats contributors-stats--${fields.length}-fields ${detailsClassName}`}>
            {fields
                .map(R.prop("prop"))
                .map(prop => <span className="table__cell" key={prop}>{contributor[prop]}</span>)}
        </CardContent>
    </Card>
)


