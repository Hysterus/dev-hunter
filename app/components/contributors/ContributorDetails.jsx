import React from "react"
import "./contributorDetails.scss"

const Section = ({className = "", children}) => (
    <section className={"profile-card__section " + className}>{children}</section>
)

const Stat = ({children, label}) =>
    <li className="profile-card__info__stat">
        <span className="stat__number">{children}</span>
        <span className="stat__label">{label}</span>
    </li>

const StatsList = ({className = "", children}) => (
    <ul className={`profile-card__info__stats ${className}`}>
        {children}
    </ul>
)

const ContributorDetails = ({data: contributor}) => (
    <div className="profile-card">
        <div className="profile-card__top"/>
        <div className="profile-card__avatar-container">
            <img className="profile-card__avatar" src={contributor.avatar_url}/>
        </div>
        <div className="profile-card__info">
            <Section className="profile-card__login"><h2>{contributor.login}</h2></Section>
            <Section className="profile-card__name"><span>{contributor.name}</span></Section>
            <Section>
                <ul className="profile-card__details">
                    { contributor.company && <li>Company: {contributor.company}</li>}
                    { contributor.location &&
                    <li><i className="fa fa-map-marker"
                             aria-hidden="true"/> {contributor.location}</li>}
                    { contributor.location && (

                        <li><a href={`mailto:${contributor.email}`}>
                            <i className="fa fa-envelope" aria-hidden="true"/> {contributor.email}
                        </a></li>
                    )}
                </ul>
            </Section>
            <Section>
                <StatsList>
                    <Stat label="Followers">{contributor.followers}</Stat>
                    <Stat label="Repositories">{contributor.public_repos}</Stat>
                    <Stat label="Gists">{contributor.public_gists}</Stat>
                </StatsList>
            </Section>
            <Section className="section--last"><a href={contributor.html_url}>Go to GitHub</a></Section>
        </div>
    </div>
)

export default ContributorDetails