import React from "react"
import Error from "../errors/Error"
import Spinner from "../spinners/Spinner"

const defaultOptions = {
    errorClassName: "",
    spinnerClassName: ""
}

export const Guard = (props) => {
    const options = props.options || defaultOptions

    if (props.error) {
        return <Error className={options.errorClassName}>{props.error.toString()}</Error>
    }

    if (props.loading) {
        return <Spinner className={options.spinnerClassName}/>
    }

    return props.render(props)
}