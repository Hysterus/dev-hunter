import React from "react"
import {Card, CardContent, CardsGroup, CardsListLegend} from "../cards/Cards"
import "./reposList.scss"
import "./repoDetails.scss"

const labelsList = [
    {prop: "stars"},
    {prop: "forks"},
    {prop: "watchers"}
]

const RepoDetails = ({data: repo}) => (
    <CardsGroup>
        <CardsListLegend className="repo-legend" labelsList={labelsList}/>
        <Card className="repo-details">
            <CardContent className="card__content--left">
                <span className="truncated">{repo.full_name}</span>
            </CardContent>
            <CardContent className="table repos-details repos-details--wide">
                <span className="table__cell"><i className="fa fa-star"
                                                 aria-hidden="true"/> {repo.stargazers_count}</span>
                <span className="table__cell"><i className="fa fa-code-fork"
                                                 aria-hidden="true"/> {repo.forks_count}</span>
                <span className="table__cell"><i className="fa fa-eye" aria-hidden="true"/> {repo.watchers_count}</span>
            </CardContent>
        </Card>
    </CardsGroup>
)

export default RepoDetails