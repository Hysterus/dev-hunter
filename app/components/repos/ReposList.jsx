import React from "react"
import {CardContent, Card, CardsGroup, CardsList} from "../cards/Cards"

import "./reposList.scss"

export const ReposList = ({children}) => (
    <CardsGroup className="cards-group--fourth-color">
        <CardsList>
            {children}
        </CardsList>
    </CardsGroup>
)

export const Repo = ({data: repo, ownerLogin, onClick}) => (
    <Card onClick={onClick}>
        <CardContent>
            <span>{ repo.owner.login === ownerLogin ? repo.name : repo.full_name}</span>
        </CardContent>
        <CardContent className="table repos-details">
            <span className="table__cell"><i className="fa fa-star" aria-hidden="true"/> {repo.stargazers_count}</span>
            <span className="table__cell"><i className="fa fa-code-fork" aria-hidden="true"/> {repo.forks_count}</span>
        </CardContent>
    </Card>
)