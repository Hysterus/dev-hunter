import React from "react"
import "./error.scss"

const Error = ({children}) => <div className="error">{children}</div>

export default Error;