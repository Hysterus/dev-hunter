import React from 'react'
import ReactDOM from 'react-dom'

import { createStore, combineReducers, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import {HashRouter as Router, Route, Link} from 'react-router-dom'
import logger from 'redux-logger'
import thunkMiddleware from 'redux-thunk'

import reducers from "./reducers"

import Layout from "./components/layout/Layout"

import "./styles/main.scss"
import ContribsPage from "./containers/ContribsPage"
import ContribDetailsPage from "./containers/ContribDetailsPage"
import RepoPage from "./containers/RepoPage"

const middleware = [
    thunkMiddleware,
]

if(process.env.NODE_ENV !== 'production') {
    middleware.push(logger())
}

const store = createStore(
    combineReducers(reducers),
    applyMiddleware(...middleware)
)

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <Layout>
                <Route exact path="/" component={ContribsPage}/>
                <Route path="/contributors/:login" component={ContribDetailsPage} />
                <Route path="/repos/:owner/:name" component={RepoPage} />
            </Layout>
        </Router>
    </Provider>,
    document.getElementById('app')
)