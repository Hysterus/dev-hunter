import {createReducer} from "../libs/reducers"
import R from "ramda"

const defaultState = {
    activeContributor: {
        loading: true,
        error: undefined,
        repos: undefined
    },
    contributorsRepos: {},
    reposDetails: {}
}

const actionHandlers = {
    "FETCH_REPOS_FOR_CONTRIBUTOR_STARTED": R.merge(R.__, {activeContributor: defaultState.activeContributor}),
    "FETCH_REPOS_FOR_CONTRIBUTOR_REJECTED": (state, action) => R.assocPath(['activeContributor', 'error'], action.payload, state),
    "FETCH_REPOS_FOR_CONTRIBUTOR_FINISHED": (state, action) => ({
        ...state,
        activeContributor: {loading: false, repos: action.payload},
        contributorsRepos: {
            ...state.contributorsRepos,
            [action.login]: action.payload
        }
    }),
    "FETCH_REPO_DETAILS_FINISHED": (state, action) => R.assocPath(['reposDetails', action.owner, action.name], action.payload, state)
}

export default createReducer(defaultState, actionHandlers)