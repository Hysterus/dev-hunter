import {createReducer} from "../libs/reducers"
import * as R from "ramda";

const defaultState = {
    loading: true,
    list: undefined,
    all: undefined,
    error: undefined,
    ordering: {
        prop: "contributions",
        type: "desc"
    },
    active: undefined,
    detailsLoading: true,
    reposContributors: {},
    activeRepo: {
        loading: true,
        contributors: undefined,
        error: undefined
    }
}

const sortingTypeToFunc = type =>
    type === "asc" ?
        R.ascend :
        R.descend

const sort = ordering => R.sort(sortingTypeToFunc(ordering.type)(R.prop(ordering.prop)))

const actionHandlers = {
    "FETCH_CONTRIBUTORS_FOR_ORG_STARTED": state => ({...state, loading: true, error: undefined}),
    "FETCH_CONTRIBUTORS_FOR_ORG_REJECTED": (state, action) => ({...state, error: action.payload}),
    "FETCH_CONTRIBUTORS_FOR_ORG_FINISHED": (state, action) => ({
        ...state,
        loading: false,
        loaded: true,
        all: action.payload,
        error: undefined,
        list: sort(state.ordering)(action.payload)
    }),
    "CHANGE_CONTRIBUTORS_ORDERING": (state, action) => ({
        ...state,
        ordering: action.payload,
        list: sort(action.payload)(state.all)
    }),
    "FETCH_CONTRIBUTORS_FOR_REPO_STARTED": R.compose(
        R.assocPath(['activeRepo', 'error'], undefined),
        R.assocPath(['activeRepo', 'loading'], true)
    ),
    "FETCH_CONTRIBUTORS_FOR_REPO_REJECTED": (state, action) => R.assocPath(['activeRepo', 'error'], action.payload, state),
    "FETCH_CONTRIBUTORS_FOR_REPO_FINISHED": (state, action) => R.compose(
        R.assocPath(['reposContributors', action.URL], action.payload),
        R.assocPath(['activeRepo', 'loading'], false),
        R.assocPath(['activeRepo', 'contributors'], action.payload),
        R.assocPath(['activeRepo', 'error'], undefined),
    )(state)
}

export default createReducer(defaultState, actionHandlers)