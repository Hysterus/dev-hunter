import contributorsReducer from "./contributorsReducer"
import reposReducer from "./reposReducer"
import activeContributorReducer from "./contributorReducer"
import activeRepoReducer from "./repoReducer"

export default {
    contributors: contributorsReducer,
    activeContributor: activeContributorReducer,
    repositories: reposReducer,
    activeRepo: activeRepoReducer,
}