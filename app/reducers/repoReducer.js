import {createReducer} from "../libs/reducers"

const defaultState = {
    loading: true,
    value: undefined,
    error: undefined,
}

const actionHandlers = {
    "FETCH_REPO_DETAILS_STARTED": state => ({...state, loading: true, error: undefined}),
    "FETCH_REPO_DETAILS_FINISHED": (state, action) => ({
        ...state,
        loading: false,
        value: action.payload,
        error: undefined
    }),
    "FETCH_REPO_DETAILS_REJECTED": (state, action) => ({
        ...state,
        error: action.payload
    })
}

export default createReducer(defaultState, actionHandlers)