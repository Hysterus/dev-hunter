import {createReducer} from "../libs/reducers"

const defaultState = {
    loading: true,
    value: undefined,
    error: undefined
}

const actionHandlers = {
    "FETCH_CONTRIBUTOR_DETAILS_PAGE_STARTED": state => ({...state, loading: true, error:undefined}),
    "FETCH_CONTRIBUTOR_DETAILS_PAGE_FINISHED": (state, action) => ({
        ...state,
        loading: false,
        value: action.payload,
        error: undefined
    }),
    "FETCH_CONTRIBUTOR_DETAILS_PAGE_REJECTED": (state, action) => ({
        ...state,
        error: action.payload
    })
}

export default createReducer(defaultState, actionHandlers)