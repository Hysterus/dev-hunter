import React from "react"
import R from "ramda"
import {connect} from "react-redux"
import {withRouter} from "react-router-dom";
import {fetchContributorsForRepoWithStore} from "../actions/contributors"
import {fetchRepoDetails} from "../actions/repos"
import {withComponentWillMount} from "../libs/factories/lifecycle"
import {Contributor, ContributorsList} from "../components/contributors/ContributorsList"
import RepoDetails from "../components/repos/RepoDetails"
import {withSpinnerAndError} from "../libs/factories/wrappers"
import {Guard} from "../components/utils/Guard"

const contributorFields = [
    {prop: "contributions"}
]

const RepoPage = ({loading, repo, contributors}) => (
    <div>
        <h1 className="page-title mobile-center">Repository Details</h1>
        <RepoDetails data={repo}/>

        <h2 className="mobile-center">Contributors</h2>
        <Guard loading={contributors.loading} error={contributors.error} render={() => (
            <ContributorsList fields={contributorFields}>
                {contributors.list.map(elem => <Contributor data={elem} key={elem.id} fields={contributorFields}/>)}
            </ContributorsList>
        )}/>
    </div>
)


const mapStateToProps = ({activeRepo, contributors}) => ({
    loading: activeRepo.loading,
    repo: activeRepo.value,
    error: activeRepo.error,
    contributors: {
        loading: contributors.activeRepo.loading,
        error: contributors.activeRepo.error,
        list: contributors.activeRepo.contributors
    }
})

const mapDispatchToProps = (dispatch, {match}) => ({
    onEnter: () => {
        dispatch(fetchRepoDetails(match.params.owner, match.params.name))
            .then(R.prop('contributors_url'))
            .then(R.compose(dispatch, fetchContributorsForRepoWithStore))
    }
})

export default R.compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps),
    withComponentWillMount(R.pipe(R.prop("onEnter"), R.call)),
    withSpinnerAndError({spinnerClassName: "spinner--big"})
)(RepoPage)