import React from "react"
import R from "ramda"
import {connect} from "react-redux"
import {withRouter} from "react-router-dom"

import {withComponentWillMount} from "../libs/factories/lifecycle"
import {fetchContributorsForOrg, changeContributorsSorting} from "../actions/contributors"
import {ContributorsList, Contributor} from "../components/contributors/ContributorsList"
import {withSpinnerAndError} from "../libs/factories/wrappers"

const onContributorClick = (history, login) => () => history.push(`/contributors/${login}`)

const ContribsPage = ({list, loading, error, ordering, history, onSortingChange}) => (
    <div>
        <h1 className="page-title mobile-center">Angular's Contributors</h1>
        <ContributorsList ordering={ordering} onChangeOrder={onSortingChange}>
            {list.map(contributor => (
                <Contributor data={contributor} onClick={onContributorClick(history, contributor.login)}
                             key={contributor.id}/>
            ))}
        </ContributorsList>
    </div>
)

const mapStateToProps = ({contributors}) => R.pick(['loading', 'ordering', 'list', 'error'], contributors)

const mapDispatchToProps = (dispatch) => ({
    onEnter: R.compose(dispatch, fetchContributorsForOrg),
    onSortingChange: R.compose(dispatch, changeContributorsSorting)
})

export default R.compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps),
    withComponentWillMount(R.pipe(R.prop("onEnter"), R.call)),
    withSpinnerAndError({spinnerClassName: "spinner--big"})
)(ContribsPage)