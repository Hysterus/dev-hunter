import React from "react"
import {connect} from "react-redux"
import {withRouter} from "react-router-dom";
import R from "ramda";

import ContributorDetails from "../components/contributors/ContributorDetails"
import {ReposList, Repo} from "../components/repos/ReposList"

import {fetchContributorDetails} from "../actions/contributors"
import {withComponentWillMount} from "../libs/factories/lifecycle"
import {fetchReposForContributor} from "../actions/repos"
import {withSpinnerAndError} from "../libs/factories/wrappers"
import {Guard} from "../components/utils/Guard"

const maxRepos = 20

const onRepoClick = (history, owner, name) => () => history.push(`/repos/${owner}/${name}`)

const ContribDetailsPage = ({contributor, loading, repos, history}) => (
    <div>
        <h1 className="mobile-center page-title">Contributor's Details</h1>
        <ContributorDetails data={contributor}/>
        <h2 className="text-right mobile-center">Repositories</h2>
        <Guard loading={repos.loading} error={repos.error} render={() => (
            <ReposList>
                {R.take(maxRepos, repos.list).map(repo => <Repo data={repo} key={repo.id} ownerLogin={contributor.login}
                                                           onClick={onRepoClick(history, repo.owner.login, repo.name)}/>)}
            </ReposList>
        )} />
    </div>
)

const mapStateToProps = ({activeContributor, repositories}) => ({
    contributor: activeContributor.value,
    loading: activeContributor.loading,
    error: activeContributor.error,
    repos: {
        loading: repositories.activeContributor.loading,
        list: repositories.activeContributor.repos,
        error: repositories.activeContributor.error
    }
})

const mapDispatchToProps = (dispatch, ownProps) => ({
    onEnter: () => {
        dispatch(fetchContributorDetails(ownProps.match.params.login))
            .then(R.compose(dispatch, fetchReposForContributor))
    }
})

export default R.compose(
    connect(mapStateToProps, mapDispatchToProps),
    withComponentWillMount(R.pipe(R.prop("onEnter"), R.call)),
    withRouter,
    withSpinnerAndError({spinnerClassName: "spinner--big"})
)(ContribDetailsPage)